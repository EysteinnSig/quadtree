#include <cstdlib>
#include <cstdio>

#include "quadtree_tpl.h"

void test_rectangles()
{
    using namespace QuadTreeTpl;
    printf("\nTesting Quadtree with rectangles\n");

    Rectangle r(-0.5, 1.5, -0.5, 1.5);
    QuadTreeTpl::QuadTree<Rectangle> tree(r, 100, 8);
    int maxcount = 100000;
    for (int k=0; k<maxcount; k++)
    {
        float x1 = rand() / (float)RAND_MAX;
        float y1 = rand() / (float)RAND_MAX;
        float x2 = x1+0.01;
        float y2 = y1+0.01;
        Rectangle rect(std::min(x1, x2), std::max(x1, x2), std::min(y1, y2), std::max(y1,y2));
        assert(tree.insert(rect));
    }

    int idx = 0;
    int hits = 0;
    Rectangle mask(0.9, 1, 0.9, 1);
    for (auto iter = tree.find(mask); iter != tree.end(); iter++, idx++)
    {
        if (is_intersecting(*iter, mask))
            hits++;
    }
    printf("Narrowed down to %i / %i (%f) object and found %i actual hits\n", idx, maxcount, (float)idx/maxcount, hits);
}





struct Circle {
    float x, y, r;
    Circle(float x, float y, float r) : x(x), y(y), r(r) {}
    Circle() : Circle(0, 0, 0) {}
};

namespace QuadTreeTpl {
template <>
bool is_contained(const Circle &c, const Rectangle &bounds) 
{
    return !(bounds.ymax-c.r < c.y || bounds.ymin+c.r > c.y || bounds.xmax - c.r < c.x || bounds.xmin + c.r > c.x);
}

template <>
bool is_intersecting(const Circle &c, const Rectangle &r)
{
    return !(r.ymax < c.y-c.r || r.ymin > c.y+c.r || r.xmax < c.x-c.r || r.xmin > c.x+c.r);
}

template <>
bool is_intersecting(const Circle &c1, const Circle &c2)
{
    float dx = c1.x-c2.x;
    float dy = c1.y-c2.y;
    float r = c1.r+c2.r;
    return dx*dx+dy*dy < r*r;
}
};

void test_circles()
{
    using namespace QuadTreeTpl;    
    
    printf("\nTesting Quadtree with circles\n");

    QuadTreeTpl::QuadTree<Circle> tree(Rectangle(-0.5, 1.5, -0.5, 1.5), 10, 4);
    int maxcount = 100000;
    for (int k=0; k<maxcount; k++)
    {
        float x = rand() / (float)RAND_MAX;
        float y = rand() / (float)RAND_MAX;
        float r = 0.005; 
        Circle circle;
        circle.x = x;
        circle.y = y;
        circle.r = r;
        assert(tree.insert(circle));
    }

    int idx=0;
    int hits = 0;
    Circle mask(0.9, 0.9, 0.05);
    for (auto iter = tree.find(mask); iter != tree.end(); iter++, idx++)
    {
        auto obj = *iter;
        if (is_intersecting(obj, mask))
        {
            hits++;
        }
    } 
    printf("Narrowed down to %i / %i (%f) object and found %i actual hits\n", idx, maxcount, (float)idx/maxcount, hits);
}


int main(int, char**) {
    test_rectangles();
    test_circles();
}