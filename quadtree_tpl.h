#pragma once


#include <vector>
#include <cassert>
#include <stdexcept>


namespace QuadTreeTpl {

struct Rectangle 
{
    float xmin, xmax, ymin, ymax;
    Rectangle(float xmin, float xmax, float ymin, float ymax) 
        : xmin(xmin), xmax(xmax), ymin(ymin), ymax(ymax) {
    }
    Rectangle() : Rectangle(0,0,0,0) {

    }
};

template <typename T>
/// @brief Is object fully contained within the object
/// @tparam Can by any type
/// @param obj to contain
/// @param bounds to bound the object
/// @return true if contained, else false
bool is_contained(const T &obj, const Rectangle &bounds);

template <>
bool is_contained(const Rectangle &obj, const Rectangle &bounds)
{
    return obj.ymax <= bounds.ymax && obj.ymin >= bounds.ymin && obj.xmax <= bounds.xmax && obj.xmin >= bounds.xmin;
}


template <typename T1, typename T2>
bool is_intersecting(const T1 &obj, const T2 &bounds);

template <>
bool is_intersecting(const Rectangle &obj, const Rectangle &bounds)
{
    return !(obj.xmax < bounds.xmin || obj.xmin > bounds.xmax || obj.ymax < bounds.ymin || obj.ymin > bounds.ymax);
}


template <class T>
class QuadNode {
public:

    class iterator {
    private:
        typename std::vector<QuadNode<T>* >::const_iterator node_iter;
        typename std::vector<T>::const_iterator object_iter;
    public:
        


        iterator(const std::vector<QuadNode<T>*> &nodes)
        {
            this->nodes = nodes;
            if (!nodes.empty())
            {
                node_iter = this->nodes.begin();
                QuadNode<T>* firstnode = this->nodes.front();
                object_iter = firstnode->objects.begin();
            }
            else {
                node_iter = nodes.end();
            }
            
        }
        std::vector<QuadNode<T>*> nodes;


        bool operator!=(iterator other) const {return !(*this == other);}

        
        bool operator==(iterator other) const 
        {
            if (this->node_iter == nodes.end() && other.node_iter == other.nodes.end())
                return true;

            return this->node_iter == other.node_iter 
                && this->object_iter == other.object_iter;
        }
        iterator operator++(int) 
        {
            while (true) 
            {
                if (node_iter == this->nodes.end())
                {
                    // If node is at end, then change nothing and return.
                    return *this; 
                }
                
                object_iter++;

                // if no more objects in current node, increment nodes and reset object iterator
                if (object_iter == (*node_iter)->objects.end())
                {
                    node_iter++;
                    // if node is at end, then return.
                    if (node_iter == nodes.end()) return *this;
                    // all valid nodes (except end) have atleast one object in them so this is safe.
                    
                    object_iter = (*node_iter)->objects.begin();
                }
                return *this;
            }
        }
        inline T operator*() { return *object_iter; }
        void get_node_bounds(float &xmin, float &xmax, float &ymin, float &ymax) 
        { 
            QuadNode<T> *node = *node_iter;
            xmin = node->xmin;
            xmax = node->xmax;
            ymin = node->ymin;
            ymax = node->ymax;
        }
        int get_node_depth()
        {
            QuadNode<T> *node = *node_iter;
            return node->depth; 
        }
    };


    template <typename Area>
    void find_nodes(std::vector<QuadNode<T>*> &nodes, const Area &search_area)
    {
        
        // Return if this node is not intersecting, we are not interested.
        if (!is_intersecting(search_area, this->bounds))
            return;

        // add node only if it owns objects
        if (!this->objects.empty())
        {
            nodes.push_back(this);
        }

        if (!is_leaf())
            for (int k=0; k<4; k++)
            {
                this->children[k]->find_nodes(nodes, search_area);
            }
    }

    bool is_overloaded()
    {
        return this->objects.size() > this->max_obj_count;
    }


    bool insert(T object)
    {
        /*
        1. if object does not fit => return false
        2. add object to node
        3. if node is full && not at max depth => split & propagate
        */

        //if (!is_contained(object, xmin, xmax, ymin, ymax))
        if (!is_contained(object, this->bounds))
            return false;

        if (is_overloaded())
        {
            if (is_leaf())
                split();
        }

        if (!is_leaf())
        {
            for (auto child : children)
            {
                if (child->insert(object))
                    return true;
            }
        }

        this->objects.push_back(object);
        return true;
    }

    /**
     * @brief Splits a node into its children and distributes objects into them.
     * 
     */
    void split()
    {
        float xmin = bounds.xmin;
        float xmax = bounds.xmax;
        float ymin = bounds.ymin;
        float ymax = bounds.ymax;
        float xcenter = xmin+(xmax-xmin)/2.0f;
        float ycenter = ymin+(ymax-ymin)/2.0f;
        assert(is_leaf());
        // upper left
        children[0] = new QuadNode(xmin, xcenter, ycenter, ymax, depth + 1, max_obj_count, max_depth);
        // upper right
        children[1] = new QuadNode(xcenter, xmax, ycenter, ymax, depth + 1, max_obj_count, max_depth);
        // lower right
        children[2] = new QuadNode(xcenter, xmax, ymin, ycenter, depth + 1, max_obj_count, max_depth);
        // lower left
        children[3] = new QuadNode(xmin, xcenter, ymin, ycenter, depth + 1, max_obj_count, max_depth);

        std::vector<T> newlist;

        for (T &obj : objects)
        {
            bool found = false;
            for (auto child : this->children)
            {
                if (child->insert(obj))
                {
                    found = true;
                    break;
                }
                
            }
            
            if (!found)
                newlist.push_back(obj);
        }
        objects = newlist;
    }

    /**
     * @brief Check if leaf.  Leafs contain no children.
     * 
     * @return true 
     * @return false 
     */
    bool is_leaf()
    {
        // if any child is nullptr this has to be a leaf
        for (int k=0; k<4; k++)
            if (children[k] == nullptr)
                return true;
        return false;
    }

    QuadNode(const Rectangle &bounds, int depth, int max_obj_count, int max_depth)
    {
        this->bounds = bounds;
        this->depth = depth;
        this->max_obj_count = max_obj_count;
        this->max_depth = max_depth;
    }
    QuadNode(float xmin, float xmax, float ymin, float ymax, int depth, int max_obj_count, int max_depth)
    : QuadNode(Rectangle(xmin, xmax, ymin, ymax), depth, max_obj_count, max_depth)
    {
    }
    ~QuadNode()
    {
        for (int k = 0; k<4; k++)
        {
            if (children[k]) 
            {
                delete children[k];
                children[k] = nullptr;
            }
        }
    }

private:
    int depth = 0;
    Rectangle bounds;
    QuadNode *children[4] = {nullptr, nullptr, nullptr, nullptr};
    std::vector<T> objects;
    int max_obj_count = 2;
    int max_depth = 16;
};

template <class T>
class QuadTree {
public:
    

    template <typename Area>
    typename QuadNode<T>::iterator find(const Area& bounds)
    {
        std::vector<QuadNode<T>*> nodes;
        root->find_nodes(nodes, bounds);
        typename QuadNode<T>::iterator iter(nodes);
        
        return iter;
    }

    typename QuadNode<T>::iterator end() {
        return typename QuadNode<T>::iterator(std::vector<QuadNode<T>*>());
    }

    bool insert(const T& obj)
    {
        return root->insert(obj);
    }

    QuadTree(const Rectangle &bounds, int max_obj_count = 100, int max_depth = 4)
    {
        this->root = new QuadNode<T>(bounds, 0, max_obj_count, max_depth);
    }

    ~QuadTree()
    {
        delete root;
    }

    void prune()
    {
        throw std::runtime_error("Prune is not implemented");
    }
private:
    QuadNode<T> *root = nullptr;

};

};