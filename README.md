# quadtree

## Tl;dr 
Quadtree spatial partition for arbitrary objects and shapes.

https://en.wikipedia.org/wiki/Quadtree

## What is this?
This is a templated implementation of a quadtree algorithm for arbitrary 2d shapes in a single header file.

## How does it works to add an object?
When an object is added, it starts at the root node. Each of the four child nodes is tested to see if they contain the object fully. If one does the object propagates down to that particular node. It will stop when there are no more child nodes able to contain the object or minimum number of objects in the node is not reached.  This means that objects do not necessarily reach a leaf node at the bottom of the tree but can be stored higher up as long as the node has not reached maximum capacity, this can save propagation time during queries.  If maximum capacity is reached, the node is split up (if leaf node) and all objects pushed to the child nodes as possible.


## How does it work to find objects?
The tree supports broad search for objects.
The tree can be queried with a mask shape and returns an iterator to objects that are owned by a node that intersects the mask. This means that there are possibly some objects in the iterator that are outside the masked shape. This is a fast broad phase filtering, so a more precise filter can be applied to the resulting objects.


## What shapes are supported?
Any shape can be supported both for inserting and querying as long as certain templated specialization is defined for overlap and contain tests.

Rectangle shapes are already supported and a code example (see main.cpp) is given on how to extend to circles.

## How do I build the example?
Use cmake and make
```bash
mkdir build && cd build && cmake ../ && make
``` 

## Example for a simple tree
Following example is for a quadtree containing rectangle and queried by a rectangle mask.

```c++
Rectangle r(-0.5, 1.5, -0.5, 1.5);
QuadTreeTpl::QuadTree<Rectangle> tree(r, 100, 8);
int maxcount = 100000;
for (int k=0; k<maxcount; k++)
{
    float x1 = rand() / (float)RAND_MAX;
    float y1 = rand() / (float)RAND_MAX;
    float x2 = x1+0.01;
    float y2 = y1+0.01;
    Rectangle rect(std::min(x1, x2), std::max(x1, x2), std::min(y1, y2), std::max(y1,y2));
    assert(tree.insert(rect));
}

int idx = 0;
int hits = 0;
Rectangle mask(0.9, 1, 0.9, 1);
for (auto iter = tree.find(mask); iter != tree.end(); iter++, idx++)
{
    if (is_intersecting(*iter, mask))
        hits++;
}
printf("Narrowed down to %i / %i (%f) object and found %i actual hits\n", idx, maxcount, (float)idx/maxcount, hits);

```

Returns:
```
Testing Quadtree with rectangles
Narrowed down to 5743 / 100000 (0.057430) object and found 1211 actual hits
```


## TODO:
- Delete using iterator:  tree.erase(iterator)
- Prune the tree (automagically or triggered?): tree.prune()